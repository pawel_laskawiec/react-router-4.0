var src = './app/',
    dist = './web/';

module.exports = {
    server: {
        root: dist + '',
        watch: dist + '**/*.*'
    },
    views: {
        src: src + 'views/**/*.*',
        dist: dist + '',
        task: 'pages'
    },
    scripts: {
        src: src + 'assets/scripts/**/*.*',
        entry: src + 'assets/scripts/app.js',
        dist: dist + 'assets/scripts/',
        task: 'scripts'
    },
    styles: {
        src: src + 'assets/styles/**/*.*',
        dist: dist + 'assets/styles/',
        task: 'styles'
    },
    images: {
        src: src + 'assets/images/**/*.*',
        dist: dist + 'assets/images/',
        task: 'images'
    },
    fonts: {
        src: src + 'assets/fonts/**/*.*',
        dist: dist + 'assets/fonts/',
        task: 'fonts'
    },
    files: {
        src: src + 'assets/files/**/*.*',
        dist: dist + 'assets/files/',
        task: 'files'
    }
};
