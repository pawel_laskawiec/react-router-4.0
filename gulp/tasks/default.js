var gulp = require('gulp'),
    config = {},
    runSequence = require('run-sequence');

gulp.task('default', function (callback) {
    var tasks = [];
    tasks.push('build');
    tasks.push('watch');
    runSequence(
        tasks
    );
});
