var gulp = require('gulp'),
    config = {},
    handleError = require('./../tools/handleError');
config.path = require('./../config/path');

gulp.task('watch', function () {
    for (var key in config.path) {
        if (config.path[key].hasOwnProperty('src') && config.path[key].hasOwnProperty('task')) {
            gulp.watch(config.path[key].src, [config.path[key].task]);
        }
    }
});
