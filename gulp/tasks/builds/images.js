var gulp = require('gulp'),
    config = {},
    changed = require('gulp-changed'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant');
config.path = require('./../../config/path');

gulp.task('images', function () {
    var result = gulp.src(config.path.images.src)
        .pipe(changed(config.path.images.dist))
        .pipe(imagemin({
            progressive: true,
            svgPlugins: [{
                removeViewBox: true
            }],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(config.path.images.dist));
});
