var gulp = require('gulp'),
    config = {},
    autoprefixer = require('gulp-autoprefixer'),
    changed = require('gulp-changed'),
    sass = require('gulp-sass'),
    urlAdjuster = require('gulp-css-url-adjuster'),
    minifyCss = require('gulp-minify-css'),
    sourcemaps = require('gulp-sourcemaps');
config.path = require('./../../config/path');

gulp.task('styles', function () {
    var result = gulp.src(config.path.styles.src)
        .pipe(sourcemaps.init())
        .pipe(changed(config.path.styles.dist))
        .pipe(sass({
            errLogToConsole: true,
            precision: 1
        }))
        .pipe(autoprefixer({
            browsers: ['last 45 versions'],
            cascade: false
        }))
        .pipe(urlAdjuster({
            append: function (url) {
                var version = new Date().getTime();
                return url + '?version=' + version;
            }
        }))
        .pipe(minifyCss())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(config.path.styles.dist));
});
