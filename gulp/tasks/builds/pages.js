var gulp = require('gulp'),
    config = {},
    changed = require('gulp-changed');
config.path = require('./../../config/path');

gulp.task('pages', function () {
    var result = gulp.src(config.path.views.src)
        .pipe(changed(config.path.views.dist))
        .pipe(gulp.dest(config.path.views.dist));
});
