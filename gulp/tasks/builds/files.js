var gulp = require('gulp'),
    config = {},
    changed = require('gulp-changed');
config.path = require('./../../config/path');

gulp.task('files', function () {
    var result = gulp.src(config.path.files.src)
        .pipe(changed(config.path.files.dist))
        .pipe(gulp.dest(config.path.files.dist));
});
