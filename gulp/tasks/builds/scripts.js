var gulp        = require('gulp'),
    config      = {},
    changed     = require('gulp-changed'),
    sourcemaps  = require('gulp-sourcemaps'),
    uglify      = require('gulp-uglify'),
    browserify  = require('browserify'),
    babelify    = require('babelify'),
    source      = require('vinyl-source-stream'),
    buffer      = require('vinyl-buffer')
    gutil       = require('gulp-util');

config.path   = require('./../../config/path');

gulp.task('scripts', function () {
    var result = browserify({
        entries    : config.path.scripts.entry,
        extensions : ['.jsx'],
        debug      : true
    })
    .transform(babelify)
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(uglify().on('error', gutil.log))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(config.path.scripts.dist));
});
