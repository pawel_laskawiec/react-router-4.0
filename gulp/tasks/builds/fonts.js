var gulp = require('gulp'),
    config = {},
    changed = require('gulp-changed');
config.path = require('./../../config/path');

gulp.task('fonts', function () {
    var result = gulp.src(config.path.fonts.src)
        .pipe(changed(config.path.fonts.dist))
        .pipe(gulp.dest(config.path.fonts.dist));
});
