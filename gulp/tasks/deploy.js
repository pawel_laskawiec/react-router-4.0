var gulp = require('gulp'),
    runSequence = require('run-sequence');

gulp.task('deploy', function (callback) {
    runSequence(
        'build',
        callback
    );
});
