<h3>Formularz kontaktowy</h3>
<?php if(!empty($message)): ?>
  <div class="modal">
    <div class="modal-content">
      <?php echo $message; ?>
    </div>
    <div class="modal-bg"></div>
  </div>
<?php else: ?>
  <form name="form" id="form" action="mailer.php" method="post">
    <input type="text" id="formName" name="form[Name]" placeholder="Imię i Nazwisko">
    <input type="text" id="formMail" name="form[Mail]" placeholder="Adres email">
    <input type="text" id="formPhone" name="form[Phone]" placeholder="Numer telefonu">
    <textarea id="formDescription" name="form[Description]" placeholder="Wiadomość"></textarea>
    <span class="btn btn--red submit">Wyślij</span>
  </form>
<?php endif; ?>
