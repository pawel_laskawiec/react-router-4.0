<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <?php include 'head/seo-meta.php' ?>
  <link href="assets/styles/main.css" rel="stylesheet">
  <?php include 'head/ga-script.php' ?>
</head>
