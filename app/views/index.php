<!-- <!DOCTYPE html> -->
<html lang="en">
  <?php include 'partials/head.php' ?>
  <body>
    <?php include 'partials/fb-ui.php' ?>

    <div id="app"></div>

    <?php include 'partials/preloader.php' ?>
    <?php include 'partials/scripts.php' ?>
  </body>
</html>
