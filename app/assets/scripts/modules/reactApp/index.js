/** @module App Index */
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { BrowserRouter } from 'react-router';
import reducers from './globalReducers';
import App from './containers/app';


const appContainer = document.getElementById('app'),
      appStore     = createStore(reducers, window.devToolsExtension && window.devToolsExtension());

if (appContainer) {
  render(
    <BrowserRouter>
      <Provider store={appStore}>
        <App />
      </Provider>
    </BrowserRouter>,
    appContainer
  );
}
