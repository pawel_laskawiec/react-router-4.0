/** @module App Users Reducer */


export default (state = [], action = {}) => {
  switch(action.type) {
    default: return state;
  }
}
