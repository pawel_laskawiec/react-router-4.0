/** @module Default App Container */
import React, { Component } from 'react';
import { Header, Nav } from '../components/Header/index';
import { Link, Match } from 'react-router';

// App Page Impoer
import GamesPage from '../containers/games';
import NewGamePage from '../containers/new-game';
import HomePage from '../containers/home';


const navLink = [
  {
    hrefLink: '/',
    name: 'Home'
  },
  {
    hrefLink: '/new-game',
    name: 'Add new game'
  },
  {
    hrefLink: '/games',
    name: 'Game'
  },
];

export default class App extends Component {
  render() {
    return (
      <div>
        <Header
          text="Welcome 2 React!"
          image="http://blog.differential.com/content/images/2015/08/react-header.png"
          >
          <Nav>
            {navLink.map((item, key) =>
              <li key={key}>
                <Link activeClassName="active" activeOnlyWhenExact to={item.hrefLink} >{item.name}</Link>
              </li>
            )}
          </Nav>
        </Header>

        <div className="container">
          <Match exactly pattern="/" component={ HomePage } />
          <Match exactly pattern="/new-game" component={ NewGamePage } />
          <Match exactly pattern="/games" component={ GamesPage } />
        </div>
      </div>
    );
  }
}
