/** @module App Header Component */
import React from 'react';

// Componetn style load
import { css } from './style.scss';
import loadStyle from '../../utils/loadStyle';
loadStyle(css);


export const Header = (props) => (
  <header>
    <img src={props.image} />
    {props.children}
  </header>
);


export const Nav = (props) => (
  <nav>
    <ul>
      {props.children}
    </ul>
  </nav>
);
