export const selectUser = (id) => ({
  type: 'SELECT_USER',
  id
});
