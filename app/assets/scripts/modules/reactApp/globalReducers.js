/** @module App globalReducers */
import { combineReducers } from 'redux';

// All Single Reducers for react APP
import userReducer from './reducers/user';


export default combineReducers({
    user: userReducer
});
