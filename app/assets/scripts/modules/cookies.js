/** @module Cookies module */
import $ from 'jquery';


// RUN MODULES
let $cookieID               = $('.cookies'),
    $cookieCloseBTN         = $('.js-close-cookies'),
    cookieName              = "proevent_accept",
    cookieActiveClassName   = 'show';

if (document.cookie.indexOf(cookieName) < 0) {
    $cookieID.addClass(cookieActiveClassName);
    $cookieCloseBTN.on('click', function(e) {
        e.preventDefault();
        let d = new Date();
        d.setTime(d.getTime() + (30*24*60*60*1000));
        let expires = "expires="+d.toUTCString();
        document.cookie = cookieName + "expires=" + expires;
        $cookieID.removeClass(cookieActiveClassName);
        return false;
    })
}
