import $ from 'jquery';
import Preloader from './modules/preloader';
import './modules/cookies';
import './modules/ga';
import App from './modules/reactApp';

$(window).load(() => {
    Preloader.hide();
});
