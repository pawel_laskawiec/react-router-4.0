Powyższe repozytorium jest mikro testem nowego 'react-router' w wersji 4.0. Jak dotąd jest to nadal wersja alpha, jednak już w tej chwili widać znaczące różnicę działania. Natomiast według opinii jest to wersja na tyle statyczna, że co niektórzy zaczynają już ją powoli stosować.

Różnice są zauważalne zarówno w strukturze samej implementacji jak i w sposobie odziaływania na DOM aplikacji. Wcześniejsza wersja react-router 'podmieniała' nam elementy domu. Nowa wersja dla każdego elementu, który ma się pojawić w danym miejscu podstawia do struktury DOM element 'noscript'. Być może to się zmieni, tego dowiemy się w ostatecznej wersji.

Powyższe repozytorium zawiera już wszystko wraz z najnowszą wersją react-router.

Do uruchomienia projektu wykonujemy:
1. Pobieramy repo,
2. Instalujemy wszystkie komponenty metodą: 'npm install',
3. Budujemy repozytorium wyjściowe metodą: 'gulp'.

W przypadku chęci pobrania najnowszej wersji 'react-router' musimy wykonać następujący kod w npm: 'npm install react-router@'

Więcej szczegółowych informacji na temat tej wersji routera można znaleźć na oficjalnej stronie projektu: https://react-router.now.sh/